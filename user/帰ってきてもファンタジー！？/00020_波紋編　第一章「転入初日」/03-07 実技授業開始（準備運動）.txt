
　信一为了做好下节课的準备而连忙跑开了，不过其他学生也是同样。
当场傻站的学生们也回过神来，非得回去上下一节课不可。
在这种行动中，大半的学生都暗中决定将刚才的事情当做没發生过，不过只有哈泽和卡露难以掩饰不悦，吐露出不满之意。
。
「那傢伙是怎麼回事！区区吊车尾的地球人！」
。
「所以说地球人都是不知尊敬强者的无礼者！」
。
「……………」
。
夹在这两人中间走动的大小姐（螺旋卷）像是在深刻思考般沉默不言。
。
对此犹如侍从一般跟随着的他们也感到奇怪，稍微提高点音量说道。
。
「帕蒂艾露大人？帕蒂艾露大人！？」
。
「诶、什麼事，丽泽特？」
。
「才不是什麼事，请问您怎麼了，在发着呆」
。
「啊、没事，只是在想事情而已」
。
她像是“什麼事也没有”地摇摇头，对此这对发色相反的随从都相互以眼神。
随後以“明知道这是不知分寸的行为”这句话为开场白，向她谏言。
。
「最近帕蒂艾露大人很奇怪。
　锻炼比起以往更加热心是件好事，不过总会尝试一些莫名其妙的事情。
　如果是以前的您，今天就会用强硬的言语让真宫寺闭嘴、
　面对那男人的无礼也应该会展现出实力的差距进行处罚」
。
男学生—卡卢特・卡鲁指出她与以往的不同。
（飒:前面出现的卡露改卡鲁）

。
「您以往都没有与地球人有任何接触，可现在却是与地球人进行交流。
　也因为如此，导致那些自我意识过剩的下º贱男人都产生误会，因而抱着轻鬆的态度向您搭话。
　您是荣耀的伽雷斯特贵族——帕蒂艾露家的继承人。
　本不应该与那些下º**士有过多的接触」
。
女学生—丽泽特・哈泽则是诉说着身为高贵人士应有的言行举止。
。
「…………」
。
即便听取着他们所说，螺旋卷—亚丽丝迪露・Ｆ・帕蒂艾露也仍是为自己心中没有因常年陪伴在身边的随从所说而有所反应，因此叹了口气。
她知道他们想要表达的意思，事实上她自己也是被这样教育过来的。
不过不知为何，在她心中这种生活方式以及价值观都已经褪色了。
。
「………只能亲自确认一下了呢」
。
所以她并没有对这两人回以任何言语，自己内心也无法巧妙地说明为什麼会变成这样子
。
。
「帕蒂艾露大人？」
。
所以她无视他们的疑问声，仿佛下定决心一般向随从告知。
。
「我想请你们两人去通知一下下节课程的老师。
　就说我要使用特别科学生的特权，既行使免除课程权。
　基於辅导教育晚辈的义务，现在要去辅佐一年级学生的体育课程！」




——————————————————————————————————

 这裡是第二运动场，与第一运动场不同，与其说是运动场，更像是竞技体育场。
在某个角落中１－D班的学生们已经换上肩膀刻有校章的体操服，排列得整整齐齐。
如果只有身为副班主任的芙莉蕾，以及一位身穿白衣的男性在这裡那倒还好。不过同样是身穿体操服的亚丽丝迪露站在老师的旁边就显得稀奇了。
虽说重点是１－D班的学生能看到学园的名人，都有些雀跃就是了。
。
「居然能够近距离见到帕蒂艾露前辈！」
。
「真的是伽雷斯特的贵族啊，一看就知道所在的世界不同」
。
「明明有着这种能力值，身体居然还那麼好……！」
。
 。
死鱼眼似乎恢復些活力一般，１－Ｄ班的学生都欢悦起来。
她身上所穿的体操服与他们之间别无二致，不过身材上丰满且呈现S型的身体曲线则是如实地显现出不同之处。
华丽与美貌相结合，无论是男生亦或是女生都将目光紧紧盯在上面。
除了某个人。
。
「───因此多纳杰老师，能请您给出许可吗？」
。
而亚丽丝迪露则是没有在意这场骚动，而是犹如理所当然一般以沉稳、贤淑的言行举止求得别人的回答。
不过话中带有不允许否定的意思。
依然是运动衫打扮的芙莉蕾则是对她的态度感到不悦似的蹙起眉头。
。
「明明就知道这边没有拒绝的权力，还真敢这样说」
。
「彼此彼此吧………那麼请问您可以答应吗？」
。
特别科的学生有义务出席晚辈的课程，在课堂上进行支援。
这是为了补足通晓双方世界的人才过少所导致的人手・知识上的不足。
为此学生无法拒绝来自于课堂上的支援请求，而老师也不能拒绝来自于学生的申请支援。
。
「哥……弗兰克老师，你觉得这样可以吗？」
。
她正想说什麼时便停顿一下，向站在旁边的白衣男性諮询下意见。
将淡紫色的长髮弄成一款随意髮型的他，满脸笑容地点点。
。
「可以的，我无所谓喔，芙莉蕾老师。
　本来我的职责就是出借福斯特和进行说明而已。
　您就请按照以往继续上课吧」
。
「…………似乎就是这样，我答应了。
　不过这次有转学生和入学一个月的学生，所以我是打算教授初期教育和複习课程来着？」
。
「那麼转学生的初期教育就交给我来做吧。
　老师就请锻炼其他各位学生，让他们複习一下」
。
面对她主动提出想承担的职责，芙莉蕾投以诧异的目光。
而亚丽丝迪露则是以捉摸不透的柔和笑容巧妙地蒙混过关。
。
「平时总是不怎麼情愿的你，在这次还真是相当积极的嘛，帕蒂艾露」
。
「不，我只是有些在意这时候难得来到这裡的转学生而已」
。
对於“你有什麼企图”的询问，回以“只是兴趣而已”。
虽然老师不能接受她这种回答，但也知道再继续刨根问底也是无济于事。先投降的是老师，她叹了口气。
。
「哈啊……中村，这样可以吗？」
。
只有一位身穿不同的体操服─素色T恤和短裤的信一则是露出迷惑的表情，索然无味地回答道。
。
「那个，只要课程内容没有改变，我怎麼样……都无所谓」
。
「这一点你根本不需要担心，中村君。
　帕蒂艾露同学在讲座上是顶级的優等生。
　实战方面也是名列前三，如果是初期教育完全是没有问题的」
。
名为弗兰克的白衣老师似乎是将信一的消极态度理解为“对於亚丽丝迪露这位学生是否能教会他自己而感到不安”。
虽然是误会了，不过纠正也是没有意义。於是他以暧昧的态度点点头。
。
「没有的事，老师。那终究只是基於学生层面上。
　与拥有剑圣之名的多纳杰老师相比之下，我还仍是未成熟」
。
得到老师的赞赏，她自满地注视着白髮女性并又谦虚地说道。
那道眼光中蕴含着憧憬之色，但也具有对抗之意。
。
「っ」
。
「？」
。
骤然间不知是谁紧咬牙齿，听到这道声响的仅有信一而已。
芙莉蕾无视他们的交谈，走到排列整齐的１－Ｄ学生面前。
随後注视着所有人，将首次课程中说过的话再次说出口。
。
「虽然这件事跟刚入学的你们没有关係，不过在这座学园裡的学生都无一例外具有“讨伐义务”。
　体育课程主要的目的并不是运动，而是为此所实施的战鬥训练。
　今天就让我见识一下你们一个月的训练成果吧。
　各自做完热身运动後就绕跑道跑五圈，然後找人进行技能对练。开始！」
。
「「「是！！」」」
。
「诶、诶诶？」
。
信一对於没听说过的可怕词语感到疑惑不解，又因为他们的眼神依然没有活力，可回应这麼响亮而狼狈不堪。
是有精神还是没精神，还是说仅仅是害怕老师。
无论如何神经都太紧绷了，他在内心如此冷静地进行批评。
。
「中村就交给弗兰克老师和帕蒂艾露你们。
　让他热身运动後就开始初期教育和试射练习」
。
如此说完，芙莉蕾就去指导其他学生们开始热身运动。
信一将目光从她身上移开，与稍远处的白衣老师和学生两人相互对视。
。
「初次见面，我是弗兰克・多纳杰。
　虽说是技术科的老师，不过今後应该也会有机会指导一下普通科吧，请多指教。
　总之先让我恭喜你平安无事地回归」
。
看样子其他的老师似乎还将他认知为归还者而已。
不然这句话听起来就只是在讽刺。
他边打招呼边伸出手，对此信一动作晚了一步。
。
「…………啊啊，请多多指教，老师」
。
正因为对方不知道事情缘由才会有股焦躁感，不过他压抑住感情跟老师握手。
手上没有多少肌肉，正如外表所见像是学者的手。
虽然手上莫名有种老茧的痕迹很奇怪就是了。
。
「啊咧，多纳杰该不会就是？」
。
「是啊，我就是你副班主任的哥哥。
　在工作上会容易叫错人，所以都是用名字称呼。
　虽说原本有名就是芙莉蕾老师，而我只是“她的哥哥” 」
。
 弗兰克露出开朗的笑容开着玩笑，不过他那金色的眼神裡没有在笑。
感觉他手上的压力都增加了，信一察觉到他表面的态度与心中所想并非是同一回事。
随後他还将目光移向在场的那位身为指导员的少女。
。
「…………」
。
骤然间，他与看向这边的少女双眼交汇。
那是犹如在确认什麼般，像是要看穿某种事物的眼神。
。
「怎麼了？」
。
「………没事，很抱歉做出这种没礼貌的行为。
　我是亚丽丝迪露・Ｆ・帕蒂艾露。
　担任特別科３－Ａ班的班长。
　在今天的课程中，是由我来负责指导你」
。
明明身穿着体操服，但却以显得优雅的举止向他打招呼。
虽然从中感觉到她有意想要蒙混过关，可即便如此举止言谈颇为自然。
这并非是一朝一夕所能练就出来的，而是长年累月所培养的。
这种即便说是成为习惯也不为过的动作已然烙印在她身体裡。
“是货真价实的吧，至少在礼仪礼节上”
连在异世界看到真货的他都会这麼想。
。
「我也必须得恭谨一点……吗？」
。
「诶、不用。请不用在意这一点，用普通的语气就行了」
。
信一不禁有所戒备，不过在得到她的许可後就安心地吐了口气。
他简单地自我介绍过後，两人都开始热身运动。
信一面向着她，像是模仿她一般进行简单的热身运动。
先是腿部的深蹲，再来是侧压腿、直立压腿、直立後仰等等。
。
「我有些问题，刚才老师说过的讨伐义务是要讨伐什麼？」
。
「从、从这裡开始吗。
　虽然是、听说过、没有知识、这还真是、严重呢」
。
她露出苦笑，心想着这样也真亏他能够入学。
虽然他理解到了这是多麼没有常识的提问，不过并没有在意。
。
「毕竟他是没有接触到这边文明的归还者」
。
「我、知道了。光是只做、热身运动、也是、浪费时间吧。
　弗兰克老师，你能不能将『辉兽』的影像资料、给他看」
。
她边热身运动，边拜托老师。而老师则是点点头操控起平板终端。

 在触碰到约Ａ４纸尺寸的屏幕後，上课时所看到的立体影像都逐渐显现在信一能看到的位置上。
。
「嘿~，这种东西还可以这样用啊」
。
「嗯、嗯……可以的话、我倒是希望、你惊讶一下影像裡面的、内容、啊」
。
信一为小型的终端能够投影出如此具有现实感的影像而感到佩服，不过其内容是可怕的。
那是恐怖电影和怪物电影中经常出现的怪物。
虽然在形态上有些个体与既有的动物以及昆虫相似，但比起它们更加凶恶，利爪也尖锐。
体格虽说根據种类有所不同，不过都比起相似的生物还要巨大。
其中也有酷似於地球歷史中架空存在的生物。
。
「这就是……辉兽吗。
　只要是辉兽，无论是哪一只都会有发光的器官吗？」
。
「嗯，按照伽雷斯特语、直译过来、嗯嗯。
　应该是魔物、怪物比较合适、不过架空存在、以这些比喻表现的词句混淆、在一起会很複杂、哈啊、据说似乎是、这样子」
。
因此虽说它们的，形态五花八门，但必然拥有着发光的器官，因此就是发光的野兽，决定给它们命名为『辉兽』。
。
「啊啊，那个弗兰克老师……就拜托你继续说明了」
。
应该是身体僵硬吧，她身体每当动弹时似乎都难以喘气。
於是信一拜托老师继续说明下去。
虽说只是因为他继续听她说下去会莫名感到尴尬。
。
「诶、我…」
。
「我知道了，那就由我来说明吧」
。
虽然不知道老师是明白他的意图呢，还是觉得这样做效率比较好，不过还是操控着图像将关於辉兽的信息简单地总结出来。
。
「辉兽这种生物无论是在伽雷斯特还是地球都是有害的。
　诞生情形以及讨伐的歷史之类的你迟早会详细学习到，因此这裡就不多做说明了。
　简单来讲，次元能量通过疑似生命体从世界外界而来，但聚集在某个区域达到一定水平时就会形成辉兽这种怪物」
。
在地球简略化的图像上，将从外部流进地球内部的能量以箭头表现出来。
随後播放出当能量集中以後成为怪物的视频，对此信一感到疑惑。
。
「哈？外部是、不对，地球也有吗？」
。
「首次得知此事的人全都十分惊讶，都说“不可能存在”。
　不过地球上世界各地都有神兽、幽灵、妖怪ＵＭＡ等这类故事不断流传下来，这些就正所谓是辉兽。只不过是發生機率相较于伽雷斯特比较低而已，因此地球人没有认知到它们的存在」
。
「……真的假的」
。
人类的确是没有走遍整个地球。
也并非已然得知地球上所有的生物。
得的的知识也是由异世界人所证明出来的，真是够打击人。
何况那是与在那边的魔物相同的怪物就更不用说了。
看到他惊讶的样子，弗兰克继续操控图像显示出一座次元之门。
图像变化为来自于地球上的剪头都指向着这裡。
。
「位於这座海上都市维托里亚的次元之门具有将形成辉兽诞生的原因，既次元能量进行收集的作用，将其吸引到学园内所隔离开来的野外区域，故意使其怪物」
。
「……原来如此，也就是说通过打倒辉兽来作为战鬥训练，并且还可以使得其他地方的辉兽产生率更加下降吗」
。
正所谓是一石二鸟的课程。
虽说在都市外有人将让未成年的学生战鬥这件事视为问题，但对於很久以前就捨弃这种日本人思考方式的他而言，倒是没觉得什麼。
不如说要是将这裡的设施和系统认为是士官学校，反而可以接受。
不过在最後他还是说出关键的疑问。
。
「可是这样一来不也就收集到来自于伽雷斯特那边的能量？」
。
「……由於机能上的问题，从伽雷斯特那边的次元之门周围也收集到不少能量呢」
。
稍微有点呆然的弗兰克平静地如此回答。
而听取的他则是回应“是吗”并微微点点头，原地跳动幾下後就结束热身运动。
。
「嘶ー哈ー……可别因为这样就有奇怪的误会哦。
　那只是细微的份量，根本不会影响到伽雷斯特的辉兽发生率」
。
她也轻轻地跳跃过後就进行深呼吸。
看来对於这件事似乎有不少人会莫名地猜疑。
对於他来说也不是真的在猜忌，毕竟他们要做肯定是做得很顺利的吧。
虽说这也是接近於疑心就是了。
。
「不过话说回来……真亏你能够毫不顾虑地尽情运动呢」
。
他眺望着做完一套热身运动後的她，呆然似的说着。
也许是因为身体的机能和构造没有什麼不同，热身运动与地球上普遍的方式没有多大差别。
不过。
。
「什麼意思？」
。
「那些傢伙都在死盯着你喔」
。
他如此说着，用手指示意在背後的跑道上跑步的学生们。
随着他的举动，她的目光也移向那边，随後有幾个人很尴尬似的别开目光。
学园的体操服作为衣服来分类的话，其实跟信一身上的衣物没有多大区别。
只不过是设计和颜色不同而已，因此裡面的不同之处便会如实地显现出来。在这种状态下进行热身运动，这些深蹲、直立压腿、直立後仰、跳动、深呼吸等诸如此类的动作都会更加强调出她那丰满的身材。
思春期的少年少女都被夺走目光。
不过本人似乎是不怎麼介意的样子。
。
「啊啊，请不要担心，我已经习惯这种事了。
　而且真要说的话，这也是代表着对我看得入迷。
　只要不是过於露骨，作为一位女性而言没有比这还值得光荣的吧」
。

 她自满地笑着说。
本人似乎也是明知道这点才会堂堂正正地展现出来。
那与其说是在夸耀，更像是将艺术品供人欣赏的感觉。
仿佛在诉说既然要成为引人注目的存在，这就是理所当然一般。
。
「………既然本人都这样说那就无所谓了」
。
信一没多大兴趣似的“哎”地摇摇头。
其中也是在同情着那些似乎跑得很辛苦的男性同学。
。
「啊啦，虽然我感觉你也总是盯着我看来着？」
。
亚丽丝迪露微笑地以捉弄人的声音回应道。
虽说都已经要展现出来了，让人观看是理所当然的，不过正因为她从他的视线中没有感觉到任何的邪念，所以才敢这麼轻率地说出口。
。
「是啊，有点移不开目光……明明那麼大，真亏你没能撞上」
。
「诶？哈！？」
。
正因为如此，他这句意料之外的言语以及端详的目光让她不禁双手交叉于胸前，将其遮掩起来。
看样子被周围人所观望跟被眼前的那个人所注视似乎并不是同一回事。
虽说原本面对他，她就有无意义的羞耻心就是了。
。
「……为什麼那麼剧烈的运动也能没摩擦到地面一次呢？」
。
「哈？」
。
「不，这也太奇怪了吧，明明就那麼大、那麼长，无论你怎麼做都不会撞上地面喔！？
　那种绝妙的避开方式只能说是活着的」
。
「请你等一下！你到底在看我的哪裡！？」
。
「诶，螺旋卷」
。
他像是“你在说什麼理所当然的事情一般”立刻即答。
本人虽说有自觉到自己的髮型具有特色，但似乎是无法接受只盯着头髮上看吧，她生起气怒吼回去。
。
「为什麼你总是要光看着那裡啊！？」

「…………啊咧，这是为什麼呢？」
。
「别问我！」
。
本人似乎不怎麼清楚似的歪着头。
当然，亚丽丝迪露也不可能知道其中的原因。
。
「不，嗯，就别在意这点小事吧，螺旋卷前辈」
。
「请不要说得像是名字一样！我的名字是亚丽丝迪露！」
。
「我知道了啦，亚丽丝罗露……好像很好吃」
。
「请别搞混了！我是什麼点心吗！？」
。
最初的优雅不知消失到何处了。
螺旋卷不顾周围的眼光向他大喊。
对此信一感到很有趣，因此继续捉弄她，於是她也完全是平静不下来───
