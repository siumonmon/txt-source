「――我本来就是冒险者的」

 　一边奔走在我旁边，内维尔同志那样说了。

 　现在我在巢穴外边。

 　不是说逃脱。

 　是巢穴时常有的去狩猎魔物。

 　要提高等级捕猎魔物还是最快的。
 　实战中技能上升很快。
 　这是战鬥团体< 八咫鸟 >的经验。

 　不过，从我的立场来看，也有犹豫的地方。
 　所谓魔物，恶神给予地上的生物与力量，作为恶神的仆人的< 八咫鸟 >的御使狩猎魔物不是很奇怪的吗？
 　谁也没问这个，稍微混乱着。

 　这麼说来，明显地受恶神影响着的< 八咫鸟 >的御使们，通过经验提高等级和技能，这是由女神的轮回系统而控制的吗？
 　还是说恶神方面也有一样的系统？
 　可是，我作为女神一侧的，像赞恩一样的恶神侧的教团首领还好说，若是少年班的爱蕾米娅和米格尔这样的，被洗脑而没有恶意地做御使的帮凶来被支配会是怎样的？

 　用我的脑袋是无论如何想不明白这些事情了，这裡再遇到女神桑时只有试着打听下。
 　上次是唐突的见面，没有準备好想问的问题，这次想全部的听明白，所以预先考虑好想问的事情做好準备。

 　结果在霍诺市没有访问成轮回神殿，女神桑说着的【祈祷】技能也没得到。
 　在巢穴也试着对女神桑祈愿了，可是有什麼特殊的条件吗，【祈祷】没学到。
 　还是必须要到神殿，準备什麼特别的供品，还是需要像在神社的二礼二拍的手礼一样的礼节性的流程？
 　我想到的可能性无非是这种程度的。
 　麦露薇也没有那样的知识。

 　哦，现在是魔物狩猎的话。

 　数日前，赞恩说，在巢穴旁边发现了翼龙。（译者：wyvern，双足飞龙，翼龙少两字，打着方便。。。）

 　作为亚龙的翼龙是飞龙的一种，不会吐息，但是以高机动性和锐利的爪牙作为魔物上位――被赋予B排位。
 　所谓B排位，是得要B排位的冒险者一个人才能势均力敌的意义。
 　所谓势均力敌，是连B排位冒险者也不一定能赢。（译者：真是水。）
 　因此，为了安全地打到翼龙，需要B排位以上的冒险者组成团队。

 　那麼，作为单体都已经很麻烦了，翼龙还有召集同类的群生的习性。
 　而且，翼龙不限于龙种，还在地下挖洞，或利用已有的洞做巢。

 　翼龙在巢穴旁边开始制作自己的巢的话，挖掘透地下，有着向巢穴内部连接的可能。

 　当然，< 八咫鸟 >的精锐们，不会迟滞到让翼龙筑巢的，毕竟会有崩塌的风险。

 　还有，如果翼龙在巢穴旁边建造巢，在那个高空飞来飞去，就非常突出。
 　近邻的城市――霍诺市的冒险者行会会为了这个问题来讨伐翼龙。
 　如果那样，巢穴就会被发现。

 　――事实上，关於翼龙的事，经由麦露薇也紧急的告知着阿尔弗雷德爸爸。
 　作为原A排位冒险者，发现翼龙时我的母亲，会立刻来探索吗？

 　重新考虑的话，朱莉娅妈妈一个人也十拿九稳地能打死翼龙。
 　虽是我母亲但也是可怕的人。

 　现在是内维尔同志的说。
 　作为我的味好烧铺的常客的内维尔同志，最近完全对我疏忽大意，这样也非常好说话了。

 「是这个也，到C排位就止住了。
 　但是，团队」

 「……是像< 八咫鸟 >这样的？」

 「接受了委托去探索的途中，受了伤。
 　也不是什麼厲害的魔物，哥布林的落，不过数量很多。
 　然後被非常尖的箭射中。
 　因为是是哥布林很普通所以算是侥幸吧，只刺中了我的脚。
 　原本关係很好的团队成员都逃跑了，只有我被留下了。
 　救了我的，是< 八咫鸟 >的御使。
 　算是恋慕吧，特别任务班的gazuro桑……哦，gazuro同志」（译者：路人不翻名字了）

 「gazuro……」

 　眼神看起来很阴险的傢伙吗？
 　无论如何看起来都不算善类。

 「此後，我在秘密的< 八咫鸟 >的圣居被治疗了。
 　不是现在的『乌鸦巢』，是之前的的圣居。
 　托他的福箭的後遗症幾乎没残留。
 　因此决定的。
 　捡回来的生命，为了< 八咫鸟 >来使用吧」

 　那样说笑的内维尔的脸确实很轻鬆。

 「但是，作为冒险者，< 八咫鸟 >的传言是不是听过？」

 「啊，确实，在外边没有好的传言。
 　但是反正传言是传言。
 　不试着看看就不知道。
 　试着听gazuro桑的话，恶神大人其实是好的神，与恶魔们作战着不是吗？
 　我拣得的生命也是恶神大人指引的结果，gazuro先生说了。
 　那样的事学习，我向御使报名了」

 「关於圣务，怎样想的？
 　关於……杀死人的」

 　我那样说，内维尔来回晃着眼看了看周围後，低声私语。

 「因为是你所以老实地毫不隐瞒地说出。
 　我不太明白」

 「嗯？」

 「恶神大人，那样的事。
 　我想着仅仅是报答恩义。
 　在教团的礼拜裡没缺席过，老实的说，我的头脑，不能很好的明白教导的内容。
 　赞恩先生和教主先生说的内容也就知道那麼一点点」

 　对内维尔的口气，认为他有迷惑。

 「内维尔同志成为了御使，在赞恩……成为首领之前」

 「嗯，是那样的！
 　赞恩先生成为首领之後，各种各样的事都变化着。
 　gazuro先生被赞恩先生中意而进入特别任务班，因为那个提拔我也成为了特别任务班所属。
 　最近的gazuro先生……怎麼说呢……」

 　内维尔在那裡切断了言词。

 「――喂，大蛇同志。
 　你相信恶神大人吗？」

 　突然，难答的问题跳了来。

 　看内维尔的认真的脸，我本能的认识。
 　如果在这裡撒谎，就丢失来自内维尔的信用。

 　我一瞬踌躇了，不过还是决定，去冒着风险试试。

 「……哦，我无法相信」

 「那样吗……」

 　内维尔嘟哝。

 「那样吗」

 　内维尔沉默了。

 　这样的时候，也只能等着他了。

 　幸好，我们是2、3名分别的移动。

 　为讨伐翼龙，全体20名。

 　包括领导人特别任务班所属的内维尔，从各班聚集着青年。
 　还有少年班，米格尔，爱蕾米娅，多娜，贝克，加上我的5人也参加着。

 　青年成为中心，因为这个翼龙狩猎，不是普通的驱除，兼做青年的提高等级。
 　少年班的多娜和贝克，好象下个月也有作为御使的首次圣务，为了避开不测尽可能先提高等级。

 　我被叫来了，不是作为少年班的陪同――恐怕是我会不会逃出的试验吧。
 　最近掌握的【气息察觉】，能感到後面隐藏着人盯着似的。
 　是听从赞恩的特别任务班的御使。

 　最年轻的我与内维尔在一起行动。
 　其他特别任务班所属的爱蕾米娅也一起，但现在，先行去侦察不在这裡。
 　爱蕾米娅才到弱冠之年的7岁，学会【气息察觉】和【隐秘术】。（译者：エレミアは弱冠７歳にして。我不确定是翻译成年仅7岁好，还是说日语弱冠用法不同？）

 　翼龙，在巢穴西北数千米的错综复杂的洼地打算架筑巢穴。
 　虽然周围被遮挡，毕竟是荒野，以高空难以注意到的行动去接近，也是< 八咫鸟 >的御使们擅长的隐秘行动。
 　分别少人数，隐蔽的接近。
 　内维尔發出< 八咫鸟 >特有的咂嘴一样的信号，向翼龙巢突入。

 　顺便来说不只此次，御使们平时穿着漆黑的乌鸦装束，基本装备後披上砂色的披风，翼龙从高空很难发现的。

 　内维尔，在岩石的下面的阴暗处向我招手。
 　我用【蹑足】跟随了内维尔。

 「……老实地回答。
 　我也想从这个教团脱离」

 　相当吃惊。
 　内维尔不是虔诚的恶神信仰者。

 「这个，因为是你所以能毫不隐瞒地说出……那样想不只是我。
 　……这样说的话，後面明白了？」

 「啊……」

 　他的朋友的事。

 「但是，我们的实力，敌不过教团干部和特别任务班的人。
 　我也是特别任务班,的，要说起来算是侦察兵，以信息收集的能力进入的。
 　意义与爱蕾米娅一样的战鬥的天才不同。
 　尽管如此，爱蕾米娅才7岁。控制的方法多少也想的到。
 　在其他的幹部裡，也有冷不防的危险的傢伙。
 　……至于赞恩。
   那个傢伙，与其说考虑取胜的方法，倒不如想办法能平安地逃跑」

 　我再次吃惊。
 　内维尔，也具体的考虑着对教团的战鬥。

 「怎麼了，大蛇同志。
 　哦，埃德加，」

 「……诶？」

 「别装糊涂。
 　要是你，对上那个傢伙能取胜吗？
 　有考虑过对策吧？」

 　被问着，我暂时考虑。

 　――和赞恩战鬥的对策。

 　当然，不断的模拟战着，至今还想不出确实地能取胜的方法。

 　以前，与赞恩的训练，比喻成格闘遊戏。
 　真的很强的，外行想出的奇计全部识破，而且还加上凌厉的反击。

 　只是――

「我……还要稍微想下，」

 　对我的言词，内维尔的眉毛挑起来了。

 「是那样吗！。那个傢伙是喜报」（译者：そいつは朗報だ，不太明白）

 「……不会被怀疑吗？」

 「特别任务班，除了我以外都是怪物成群。
 　只是赞恩是卓越异质的。
 　并且，我想，埃德加，你以另类的意义来说也是异质的。
 　大致赞恩察觉到了。
 　有能杀自己的可能性的《无底》」

 　《无底》。
 　《无底的大蛇》这个第二名吗？（译者：底無，可以翻译成无限、无盡，意思上就是男主的战鬥潜能看不到边界。我想翻译成深渊的大蛇啊。多麼中二！！）

 「被这麼说还是有点痒痒的……会努力的试着做」

 「好吧。
 　如果你那样说也壮胆了。
 　――我们，有耐心的决定等到那个时候」

 　说起内维尔，从岩石暗处窥探着裂隙深处。

 　内维尔的视线的前方，数百米的地方，有从这裡看上去象大拇指的指甲左右的块状物。
 　灰褐色的那个东西，考虑大小的话是3米的高度，似乎很坚硬。
 　翅膀折叠起来很难看清楚轮廓，那个似乎是翼龙。

 　翼龙在这一带的大地的裂缝边缘蹲着，耷拉着头。
 　时常的鸣叫着。

 「……这到底？」

 　失望的内维尔嘟哝着。
 　确实说翼龙打算建造巢。

 　我们确认了翼龙之後，从翼龙周围的岩石的阴处，發出闪闪地光。
 　使用的设备，是带把儿的小镜子反射太阳光用来发送了信号。
 　这样的共计5处，内维尔也说了。

 「大蛇同志，爱蕾米娅一返回就开始做。
 　其他人也在，为了防止翼龙跑掉，需要共同行动」

 「了解。……但是」

 「嗯？什麼？」

 「爱蕾米娅去哪裡了？
 　在翼龙呆着的附近也没看到」

 　先行的爱蕾米娅，在这附近没看到，很是奇怪。

 「……啊！」

 　内维尔因翼龙而惊慌着，御使隐藏到岩石阴处。
 　然後，看向裂缝的深处――

　gugyaaaaa！

 　与複数的鸣声一起，裂缝的深处，稍微塌陷而看不到的地方，数只翼龙飞出来了。
 　飞出的翼龙相拥着，御使们跟在後面展开了袭击。
 　一边混乱一边也开始应战，到底是< 八咫鸟 >的暗杀者，翼龙的皮肤不是很硬，刀的话能造成损伤。

 　被袭击的御使，以及放出【投枪技】和【投斧技】的御使们，由於是混战也分不清是谁的技能。
 　翼龙战鬥的定式所需的，拘束网也拿来了，这是狙击在地上的翼龙来用的，一旦飞起来了就无法把它们拖到地面上来使用。

 　最初的翼龙袭击过来。
 　那个巨大的脚举起正下方的大岩石，打算丢落在混乱的御使们的头上。

 「内维尔同志！」

 「――咕！全体人员，攻击开始！
 　救出gonzakku和lathere後撤退！」

 　认为隐藏已经失去了意义後的内维尔下令。
 　从各处岩石阴处，穿着和我我同样沙色的披风的御使们出来，向翼龙放飞魔法和刀，打算引起注意。

 　我在附近的岩石用【物理魔法】飘浮起来。
 　要是【念力】能更轻鬆地浮起因为是我的王牌之一所以不想显出。

 　不管怎样，例行的用魔法举起大岩石扔去。
 　【投掷术】也发挥了效果，我的岩石以直击目标的轨道，打中了翼龙的下肢。
 　翼龙拿着的岩石掉落下来，坠入裂缝深处。
 　从这裡看不到深处，不过發出轰响声回响着，看来裂缝很深。

 「做的很好！」

 　御使们和内维尔那样说的期间，也發生着变化。
 　最初被袭击的御使――gonzakku和lathere的也脱离了困境，用魔法和飞刀等各自的武器牵制着空中的翼龙。
 　魔法是是单纯的《火焰比特》，刀要划伤但是力量不足且又被躲开，翼龙们也很好的做着。

 　御使们站成一个圆圈，一边牵制空中的翼龙一边慢慢地到这边。
 　在我和内维尔的背後有一点灌木的杂木林，後面是多石的山上长着象仙人掌一样的草木丛生的长的树。
 　翼龙好象讨厌那个树，到那裡就能逃跑後重整体态。

 　还有一百米就能合流了，那样想着，爱蕾米娅也返回了。
 　是刚想着去哪裡了，发现了她从最初的翼龙飞出来 裂口回来的。
 　从裂口无声的以全速跑到这裡。
 　原本戴的沙色的带帽衫丢失了。
 　荒野上的爱蕾米娅的银色的短发随风飘动着，在内维尔面前停止了。

 　爱蕾米娅呼吸也没有乱，替代的内维尔看起来困苦地按住了心臟附近。（译者：哈哈。）

 　那是【疲劳转移】？
 　因为【不易不劳】我也没有影响。

 　内维尔也是知道爱蕾米娅的技能吗，呼吸慌乱却问着其他的事。

 「咕……爱蕾米娅同志！去哪裡了！？」

 　从状况可以理解，但内维尔的言词稍微有刺。
 　可是爱蕾米娅也没畏惧，倒不如呼喊着。

 「――这裡，不是翼龙的巢！」

 「哈？你在说什麼！现在翼龙也出现了好幾只――」

 「因此，翼龙之类的不是问题……受伤了！」

 「什麼！」

 　对内维尔的言词，爱蕾米娅打算回答的瞬间，地面摇摆不定了。
 　哦――

「倾斜了！？」

 　裂口左右的伸展着。
 　同时，我们这一带的地面很大地陷落，裂缝向周围蔓延。

 　并且那个裂缝中，火焰突然刮起！

 「是吧――」

 　在我没词的期间，爱蕾米娅呼喊。

 「是火龙！　这裡是火龙的巢！　在这一带的地下，火龙栖息着！」

 「是火龙吗！」

 　内维尔的脸上血色全无。

 「全体人员，下来！
 　脱离！」

 　对内维尔的言词，御使们向这边跑来了。
 　幸好，由於火焰的原因翼龙们向高空逃跑着。

 　不过，问题不是那。

 　由於刚才的火焰，我们站立的地面全部陷落，而且，向裂口慢慢倾斜。

 　怎麼说――因为刚才的火焰，从地下整个挖通了向我们的地面――？

 　如果说是火龙的话应该做得到吧。

 　但是，幸运的是滑坡不是让人很难行动。
 　被锻炼的御使们的脚勉勉强强地应付着尽量的避难。

 　不过，

 「――啊！」

 「多娜同志！没问题！？」

 「笨、笨蛋！　不要停！」

 　贝克跑到跌倒的多娜跟前，米格尔也迷惑的跑回去。
 　多娜的脚被绊了，是我刚才向翼龙扔去的岩石的破片。
 　并且那个瞬间，附近地面急剧地倾斜着，滑坡快速变陡了。

 「――可恶！」

 「大蛇同志！来不及了！」

 「笨蛋，回去！大蛇同志，爱蕾米娅同志！」

 　跟着飞出的我，爱蕾米娅也来了。
 　内维尔惊慌着，不过没抓住爱蕾米娅。

 　我發出左手手甲中的钢线，捆上多娜和贝克和米格尔。

 「啊！」
 「哇！」
 「哟！？」

 「爱蕾米娅抓住我！」

 「呃、嗯！」

 　滑坡终於崩塌了，我们被抛到空中。
 　被我用钢线卷上的3人以【念力】让我和他们，以及我紧紧抱住的爱蕾米娅接近着。

 　不过由於惯性，即便是传说级技能，短时间也不能让5人逃脱。

 「咕――」

 　拥挤着向滑坡下坠落，不顾一切的用手指连续的使用着【地精魔法】。

 『我也来帮助！』

 　遮住身姿的麦露薇，用【精灵魔法】呼吁精灵过来帮我们处理砂土和岩石块。

 　感觉过了很长的时间。

 　与砂土的战鬥，实际上也没到十秒。

 　我和麦露薇以及少年班总计5人在製作出岩石的球上成功稳住了。

 　那个岩石球以可怕的气势冲向滑坡深处，最後gogan的發出厲害的声音破裂成两半儿了。
 　冲击的大半用【念力】使身体飘浮就能缓解，尽管如此还是受到了部分冲击伤害。（译者：それでも鞭打ちになりそうな衝撃だった）

 　不等外边静下来，我们从岩石球的残骸中爬出来，确认了状况。

 　好歹是掉到裂口底。
 　在深的山涧底，上面砂土和岩石块以可怕的气势倾注下来。

 「――那裡！快跑过去！」

 　爱蕾米娅指着岩壁的一部分呼喊。

 　那裡有好象是洞窟的入口。
 　现在不管怎样，是能避开来自上面的飞来物的地方。
 　我们惊慌向洞窟跑进了。

 　向下的滑坡持续着。

 　後来，声音停止了，紧接其後，一刹那热风刮过，瞬间滑坡下面轰响声回响着。

 　这个，搞不好，是逃进了火龙的吐息的通路……？

 「啊，不要紧。
 　因为火龙是在反方向所以，吐息不会来这裡……大概」

 　爱蕾米娅，观察我的脸色那样说了。

 「大概？」

 「呜，嗯……。
 　我所知道的，火龙使用吐息制作自己的巢。
 　首先用吐息制作正中的深的纵穴，作为中心的巢。
 　然後，从那裡向四方八方吐出吐息，在巢的周围塑造着」

 「……那个，不会塌？」

 「吐息的炎热能简单地溶化岩石，火龙利用那个，溶化地下的空间兵改变形式，然後等待冷却固定。
 　或者上年纪的火龙，利用吐息的性质，好像也会在巢周围建造複杂的迷宫。
 　在全世界，所谓『洞窟』的幾个中，也有火龙的巢穴中也栖息着魔物」

 　是的。
 　『阿巴顿魔法全书』中也记载了。

 「因此，考虑巢的位置的话，这个地方会成为吐息h的死角。
 　吐息是从巢孔斜上而喷出的，这个深度与这个位置，吐息会向我们头上飞去，被直击是不大可能了……吧」

　 爱蕾米娅没有自信的说着。
 　语调像是靠不住似得，感到底气不足。

 　米格尔打破气氛说着。

 「……但是，吐息很斜的来往着通过上面的岩石，不是在这裡的斜上方被覆盖而困住的感觉吗？」（译者：ここの斜め上を覆いかぶさる感じで 固められちまうってことなんじゃないか）

 「…………」

 「…………」

 　我和爱蕾米娅没词了。

 　对条理清晰地推理而吃惊……比起那个还是对米格尔说内容困惑着。

 　我惊慌地跑到入口，


 　dogaan！


 　發出大的声音，入口被巨大岩石封住了。

 　咕！

 「 Ω  ∨  ――《隧道》！」

 　使用着在遗迹发掘中锻炼的【地精魔法】！

 　我的魔法在巨大岩石正中制造出了大洞。

 「噢！」

 　米格尔高声欢呼。

 　嗯，看起来不错。

 　我高兴的挺起胸后――


　juba――


　一瞬的闪光，我开出的大洞，大岩石被消灭了。（译者：岩石不是被男主魔法消灭的，是熔岩）
 　哦！

 「撒，快下来！
 　下来到深处！」

 　我喊着，从入口流入的熔岩清晰可见，少年班的4人逃出了。

 　好象在天然的洞窟内跑着的心情。

 　幾次的拐角弯曲着，来自背後的声音也听不见了。

 　全体，在那个场合停了下来，很大地叹气。

 　……连不会累的我，都做了同样的事。

 　我拿出腰间的水壶喝了些水，提心吊胆地沿着来的道试着返回。
 　米格尔们也默不作声跟我来了。

 　熔岩在弯曲的一角凝固了。
 　虽然还有些赤黑，也是快要固定的样子。

 　暂且，没有被熔岩吞没掉而死，可是


 「――嗯。被困住了？」


 　对多娜的言词，我郑重地点头了。

