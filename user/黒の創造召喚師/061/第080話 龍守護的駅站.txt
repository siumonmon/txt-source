在索亚菈和奇莉娅以及十六夜前去救援之後，阒无一人的客栈的桌子上，有一只白龙發出了“拉呦”的寂寞叫声。「希望你能守护继那的归处」虽说在三人这麼拜托时情不自禁地發出像是在说「交给我了」这样刚强的声音，但果然三人离去之後客栈空旷旷的，散發出一股哀愁之感。

“拉呦”

“好闲啊~”，昴用尽全身力气如此长叹，随後他的视线落到了低着头的修蒂尔菲脸上。对於她表情中掺杂着对女兒安危的担憂和对三人的不安，昴只是用漫不经心的哈欠声来回应。

钟表的指针一点一点走着，突然间昴抬起了头。那对蓝色的眼睛猛地睁开，全身的毛都竖了起来，并发出“唔噜噜噜唔”的低吼。它张开合上的双翼，笔直地冲向屋外發出“咯嘎”的叫声，一道覆盖住整个驿站的屏障被打开了。

“切，被发现了麼。”

在使用「远视」技能侦查到冲出室外的昴之後，一个男人發出不甘的叹息。在他的周围散立着数个男女，都發出惊讶的声音。

“开玩笑的吧？这裡离那个驿站可有200米的距离。”

“已经确认到那条白龙张开了像屏障一样的东西。不会错的。它察觉到了我们的气息。”

他恨得咬牙，支吾地陈述着所见的事实。众人见他这副模样，也最终接受了现实。

“可恶！ 屏障的话，把那条龙杀了就会解除了吧？”

“看起来是这样。不过看上去那条龙还很幼小。如果袭击成功，我们就可以享乐一辈子了。”

似乎是明白了这个满脸窃笑男人的意图，一股疯狂在这帮人之间感染。虽说幼小，但龙仍旧是龙。只要将其打败——一他们想象着这样的情景，表情自然间舒缓了下来。

但是，能否将其实现就还是题外话了。这帮袭击者却并未意识到这麼简单的事情。他们将阻挡在前的昴设为目标，向它跑去。在漆黑的街道上有三个奔跑着的人。他们都裹着黑袍，脸上缠着头巾。

“火焰怒击！”

“疾风之投枪！”

冲锋在前的男子左右传来了一男一女的声音。

在那之後，炎弹和风枪幾乎同时被释放出。开头是牵制，其次是索命。不管是什麼生物，都很难应对如此间不容发的攻击。他们凭借着积累至今的经验，而深知这一点。更何况对手尚幼。可以这麼断言，接下这招的对手绝对会身受重伤。

像是要将一切吞噬一般，饱含杀机的枪弹笔直地朝眼前的昴和它身後的屏障飞去。风之枪的长度足以贯穿昴的身体，要是被直接攻击必然会受到致命伤。

但是，面对眼前的威胁，昴悠然地张开翅膀漂浮在空中。在两者相撞的瞬间，昴发动了自身的技能。

“嗞~~~~。”

尖锐的叫声哧哧地摩擦着空间。经受不住那种摩擦，火焰瞬间散去，枪的大小和威力也衰减下来，在昴挥动的翼膜之下啪唧一声失去实效。

“那是什麼呀！”

“怎麼会，只凭一吼就……”

袭者们不禁如此惊叹道。对他们来说虽然很是痛心，但对於眼前的景象，他们不得不开始胆怯。

昴释放的的是固有技能“龙霸之威压”。但不能因为仅仅是威压就轻视它，这个技能还附加着魔法衰减效果。如果等级足够高，光凭威压就可能驱散任何魔法。尽管使用这个技能要付出代价，也不得不说这简直是作弊技能。

虽然是区区一击，但却带有明确杀意的攻击。昴的态度陡然一变。

众人眼红应该被击杀的对象——那只幼龙开始了行动。

可能是受到威压的影响，袭击者们脸上冷汗直流。其立场在瞬间从「狩猎者」转变为「被猎者」，他们头脑中立刻浮现出了撤退的念头。然而，此时的昴像是在模仿继那平日的模样，「不可饶恕」 张开了小嘴。

“可恶，这样下去可不行，快跑！”

可能是察觉到了昴的态度骤变，领队模样的男子当机立断下令道。对那个声音做出回应的两个人正要逃跑的瞬间——

“吼”

龙哮轰鸣。

浓密的魔力将周围覆盖，微暖的风拂过袭击者的腿和脸。星光璀璨的夜空被云覆盖，有什麼东西和雷光一同激射而来。

“什麼——啊啊啊！”

“呜啊啊啊！”

“哇啊啊啊！”

强烈的疼痛和灼热，还有摇撼脑髓的麻痹感席卷全身。袭击者们的惨状不忍直视，烤焦的气味充斥着鼻腔，他们的视界逐渐变得模糊。领队的男子最後所看到的，是那只小白龙注视而来的冰冷目光。

昴所释放的魔法是被称为“雷雹雨”的水雷双属性复合魔法。虽说是雹，但却不是小球状，而是尽管很小但却有着20cm长的冰锥。再加上每一支都附着雷电，单单被击中就会全身麻痹。交缠着雷电的冰锥从天而降的光景，与其说是惨剧，不如说这已经是小型天灾的级别了。

“呦噫呦噫？”

注视着眼前刺中地面的道道冰锥，“他回来了会不会夸我呢”昴不住地鸣叫着，并开心的呼呼振翅。

------------------------------------