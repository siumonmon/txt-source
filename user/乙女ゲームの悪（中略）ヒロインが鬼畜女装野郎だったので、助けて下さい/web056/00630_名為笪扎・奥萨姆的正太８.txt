　さて、やたら危ないオーラを纏いながら距離を詰めてくるトリエットを、適当に振り切り（あれは、獲物を狙う肉食獣の目でした…）、向かうは図書室　あふたー　ざ　すくーる。
　昼は、マジメガネイベントに邪魔されて、結局ダーザと接触出来なかったので今回こそリベンジ。

「…んー。これ、どこにいんだ。ダーザ」

　いつもは（外から見る限り）そう、人がいない図書室だが、テストが近いこともあって、図書室にはたくさんの生徒が密集していた。こんな中にいたら、ただでさえ、背がちっこいダーザなんてすぐ埋もれてしまいそうだ。
　ダーザのいそうなポジションは、どこかな。土属性だし、植物学関連かんな…しかし、流石私有名人。やたら視線集まってて、照れるぜ…。うん、動きづらい。

「——有股……很香的味道……」

「诶？」

被意想不到的声音，小小的吓到了。

移动视线，看到的是一如既往没有受到召唤就自顾自的出现的诺姆。

怎麼了。虽然被突然之间的声音惊讶到了，诺姆啊。又一次，自顾自地……

……诶，诺姆？

「这边……好闻的香味……令人平静，的气味……想闻，更多」

「等、怎麼回事啊这个，平时没有的迅速行动力。你一直以来的慵懒去哪裡了」

「这边……」

「喂喂，别无视主人啊！！」

土属性精灵，诺姆。
是打心裡喜爱着睡眠和怠惰的，有气无力类型的性格。
这样的他为了什麼而自己移动的情况幾乎没有，出现在这边的世界的时候，也大多是被其他精灵带着的感觉。
这样的诺姆，现在，一反常态的主动地活泼地行动。……等、你啊,是真的發生了什麼了麼。

「等下，诺姆。人很多，不要随便地走动……诺姆？咦，去哪裡了？」

身体娇小的诺姆不知飞到哪裡去了，在人海之中丢失了身影。

糟糕。不对，虽说诺姆和血气旺盛的萨拉姆【※火之精灵】不同，不是一离开视线就会引起问题的孩子，但是被周围的人看到自己没有做精灵的制御的話，非常地不妙。

虽然我和精灵们结下了主从契约，但基本上他们自己的行动都是尊重於他们的意志的。这些事长年来争论的最後，我所决定的相处方式。
并不是说不能完全地制御，只是没有什麼事的話，就不强行控制他们。
正是如此，我认为才能让被强硬的签下了契约的他们，看到我的诚意。

我与精灵们的关係，绝对不是我處於绝对优势的位置就成立的关係。
我认为这样就好了。因为在心裡这麼想着的时候，经常会确实地感受到与可爱又可恨的精灵们缔结了羁绊。
比起扭曲意志的绝对服从，现在的关係，更好。

我对於现在于精灵们的关係打心底的满足。
……但是，现在我对待精灵的姿态，如果被其他人见到的話就会有别的问题了。

大多数的人，特别是持有力量的人，有着认为精灵是比人类低一等的，道具那样的存在的倾向。
使役着他们的人也有，让他们拥有自己的意识并给予自由而被世人认为愚蠢的人也有。

那样的人看到我和精灵的关係的話会作何感想
。……为四体人形精灵所服从的人最终也不过如此么，会变成侮辱我，侮辱伯雷亚家的情况。这会使伯雷亚家的声誉受到损伤。

正因如此，我在人前与精灵们接触的时候，必定以高傲的态度对待。
精灵们，也顺应我的意图，一直都为我压抑着自己的本性而展现出顺从的姿态。
……嘛，虽然经常有不能演的很好的时候。
比如说是萨拉姆啦，和萨拉姆啦，还有萨拉姆啦。

就是这样的精灵们，说实話这次的诺姆的暴走完全是预想之外的情况。
在被连精灵都制御不好这样的不光彩的标签贴上之前，要尽早地找回诺姆才行。

「——……呜哇！？这，那个，人形精灵！？怎麼会，在这裡！？」

「土元素的香气……心情、舒畅……你、现在开始……我的枕头……」

「哇啊啊！？不要在我的头上睡觉！！给我等下，你……！！」

「……呼呼…」

……怎麼找都找不到的样子，却听到了一番对話。而且，还是意外十分近的地方。

顺便，还找到了之前在找的人的样子。
叹了一口气後，接近目标。

「——十分抱歉呢。我的精灵，给你带来麻烦」

我，向被突入休息mode的诺姆黏在头上的，處於半哭状态的笪扎・奥萨姆微笑地搭話。
