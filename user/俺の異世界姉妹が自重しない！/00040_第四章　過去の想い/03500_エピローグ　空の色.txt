

某个晴朗的午後。
我在苏菲尔家的阳台仰望着天空。
万里无云、蔚蓝澄澈的天空。
微风和畅、和煦的初夏阳光射下。
这本该是让人心旷神怡的好天气……但事实上，很想叫（这天气）读懂此刻我心中的氛围。
就结果而言，艾莉婕女士的一命是保住了。
服下十倍致死份量的砒霜仍得已救活。那简直就是奇蹟――也不能这样说。
虽没收了匿藏在调药室的砒霜，但被确认出当中含有相当量的杂质。不如说，是只混合了一成砒霜程度的代用品。
也就是说艾莉婕女士服下的砒霜份量在致死量的极限边缘。其份量假若置之不理的话，虽会确实致死，但处理得当仍能充分获救。
正因如此，才能因为我们的处理而保住一命。
――然而，实在是没能做到不留後遗症。
艾莉婕女士现在、正为药物中毒所引起的症状而痛苦着。
「在这样的地方做什麼？不是跟埃里克先生在商谈着么？」
是爱丽丝的声音。
把视线从空中往下望向身侧。看到爱丽丝正好、跟我并排靠在阳台上的栅栏。
「商谈结束了。现在只是……总觉得很想要仰望天空而已」
「是吗……结果怎样了？判决了药师的下场对吧？」
「赛思监禁在自身的房间内。但本人仍有意愿的话，今後也可以为苏菲尔家工作」
「……认真的么？」
「当然不允许制药。仅是、让他为了将其技术传达给後世而幹活」
「药师的话还有很多的吧？不管有着怎麼的内情，他可是打算毒杀掉索菲亚酱的母亲耶？」
「就是那个索菲亚原谅的哟。说道『想要杀死妈妈这点不可饶恕。然而，你所承受的悲伤索菲亚・深・有・体・会・因此活下去偿还罪孽吧』」
简而言之，就是由於用恩惠亲身感受过赛思的悲伤，因而产生了同情――虽然我这样想着，但见爱丽丝窃窃笑了。
（澪：的确有一幕描述索菲亚追体验完後仿似崩溃般地脱力+流泪呢）
「……怎麼了？」
「想着变的越来越相像了呢」
「相像是指……什麼意思？」
「没发现么？索菲亚酱的原谅方式、跟利昂如出一辙呦」
「是、这样吗……？」
被这麼一说，也不是没这样觉得呐。
……是吗、跟我相像吗。嗯，该怎麼说呢、这感觉挺不错的。
「利昂、脸放鬆下来了呦？」
「也没什麼不好吧。听到喜欢的女生受我所薰陶，感觉太开心了」
「呼嗯……？说起来……所谓的男人。对女人厌倦之後、貌似会以自己的偏好培育下一任少女呦？」
恶作剧般的眼神。
咀嚼那一句的意思後――我会心露出了笑容。
「原来如此。爱丽丝是在担心自己到底是不是被厌倦了吗？」
「――什！？才、才才才、才没有呦！？」
由於被调侃，才想要反击的说……难道说正中靶心了吗？感觉反应得有点太狼狈了。
「放心吧。我无论是爱丽丝也好还是索菲亚也好，两人都会（同等地）重视的」
「……总觉得，真是何等过份的对白呢。对往後、利昂会不会成为一个渣男、稍为有点担心耶」
「爱丽丝有资格这样说吗？」
的确要说我在脚踏两条船也不是言过其实。然後不久的将来，说不定会脚踏三条船。不、变成那样的可能性相当之高吧。
然而……我也曾经想要只注视一人。
爱丽丝也好、索菲亚也好、克蕾雅姐也好、虽然都很重要，但硬要从中选一人的话――我选择了爱丽丝，并曾想只特别对待她。可将（只选一人）的规限除掉的正是爱丽丝本人。
纵使被其他人批评我（对恋爱）不诚实也是无可奈何的，但偏偏就是不该被爱丽丝这样说……什麼的、虽然我也明白爱丽丝想表达什麼呐。
然後，爱丽丝自身也都理解着其意思吧。
「……真是的、坏心眼」
用仿如闹别扭的语调呢喃，以头“咚”的撞向我的肩膀。
虽然在这世界与爱丽丝重逢的时候她的身体远比我要大的多……不知不觉间（被我）反超了呐。
「……然後，那个索菲亚酱呢？」
「在艾莉婕女士的身边哦」
「能够对话了麼？」
「虽然受梦魇所困的时间比较多，但好像不时都有在聊天哦。刚才去看了一下情况，她说有好好地跟母亲和解了」
「这样啊……索菲亚酱、不要紧吗？」
「没事的。现在虽然会比较艰难也说不定，但艾莉婕女士的症状、理应会随时间慢慢恢復的」
「……有根據吗？」
「才没有那样的东西。只是，因为索菲亚如此坚信。所以我也那样相信着而已」
听到我那样的回答，爱丽丝浮现了些许愕然的表情。
「也许、艾莉婕桑就一直那状况也说不定呦？唔唔。岂止如此，甚至会更严重也说不定。你明白么？」
「啊啊。我知道」
只要乐观起来的话这都不成问题。
然而，无论过去多久仍未见病况转好――更不用说，日复一日目睹其逐渐衰弱更是教人非常、非常难受。
我和爱丽丝、在前世都被迫了解到不想再更了解了。
不过，尽管如此――
「没事的」
「……那根據呢？」
「我们都没事（挺过来了）对吧？」
仅凭那一句就理解到是在说前世的事吧。爱丽丝她轻轻叹息。
「我没事、是因为有裕弥哥哥在的关係呦？」
「我不也是因为有纱弥在才没事的。然後，现在的索菲亚不是有我们在么？」
「是吗……也是呢」
“艾莉婕女士终必会痊癒”.对此坚信不移的结果，也许哪一天会让索菲亚被囚禁在悲伤的牢笼之中。
那时只要我们想办法解决就行了。因为我觉得（所谓的）一家人就是这样的。
「而且，我认为不用这般担心也是没问题的。因为、虽说病况不太乐观，但呈现的幾乎都是自然的状况呐」
「也是呢。说不定就是那样」
在前世的世界，即便一部分身体的机能停止，也可以用人工的方式补足那些机能。就算那机能、是不能靠自然痊癒的部分。
可是这世界则不然。
虽美其名为处理，但充其量也只是大蒜的摄取、或是服用滋补养身之药物的程度。
在这个世界当中所谓的危险状态，在前世也不过是可以确实能获救的水平而已。况且也有世界树的树叶在，恢復的可能性理应颇高的。
「而且不管陷入什麼样的状态，我觉得亲人仍健在这点是值得欣喜的。老实说，我都有点儿羡慕索菲亚」
因为我已经再也没法知晓父亲的想法了――想着、我无声呢喃道。
「说起来……你有说过。『不用恩惠读心的话，你也会像我那般後悔的』」
「因为我……没能跟父亲言归於好呐」
「难道、这就是扫墓之後露出那异样表情的原因？」
「啊啊……嗯。事到如今这些事都已经无从稽考了。（那时）不想害爱丽丝为我担心所以沉默了呐」
「是吗。我还在想『既然艾莉婕桑的事不是主因，为什麼现在仍没精打采呢』。明明（那时候）说出来的话，马上就给你解决了」
「……哈？」
没能理解什麼意思。
因为、父亲留下的，就只有「你可是老夫自傲的兒子啊」这一句话。考虑到是那个状况的话，实在不知道那是否出自真心。
然而，爱丽丝她「我认为罗伯特先生可是很珍视利昂的呦」如此断言道。
「……你有、你有什麼根據这样说？」
「因为我有着气息察知的恩惠呢。虽然只是偶尔，但我可是知道有从远处窥视着这边的男人在的」
「那……你想说就是父亲吗？」
「虽说最初并不知道那是谁，但在宅邸见到罗伯特先生时马上便明白了。默默注视着利昂的就是这个人」
宛如嵌上缺失的碎片一样，我心中形形式式的疑问全都迎刃而解。
幼年期，时隔数年的再会那时，父亲他无论是一眼认出了我也好、在流感事件中相信了我也好、其事件过後并没有处罚我也好，全部――
全都、是因为对我珍而重之、一直……守护着我的关係。
「……这样啊。父亲他……原来有、好好地注视着我的啊」
知晓了原以为再也无从得知的父亲之想法。
心中怀着那事实，我靠在阳台抬头往上望。
在那裡――有一片万里无云、蔚蓝澄澈的天空在。
