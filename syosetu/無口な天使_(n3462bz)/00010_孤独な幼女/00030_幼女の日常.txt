
「克莉丝大小姐，已经是早上了哦~。早上好。」

克莉丝的早晨虽比不上前世，但也是很早，每天都是从乳母梅诺特的问候开始的。

窗帘伴随唦的一声被拉开，耀眼的朝阳照在了好似埋在硕大的床上睡觉的克莉丝的侧脸上。

「……呜……」

克莉丝呻吟着，仿佛要逃避日光般想要翻过身去。
但是，跟往常一样，这次也被乳母梅诺特温柔的抓住，阻止了。

「好吧好吧，大小姐，失礼了。」

克莉丝不知是因为每天修行到很晚，还是因为在这年龄下就是如此，早上很容易起不来。

虽说如此，但乳母梅诺特却并不知晓这一点，在她眼中克莉丝是一个经常在睡觉的省事的孩子。
她仅是对克莉丝抱有这种程度的认知而已。

「好了，睁开眼眼起床了哦~。」

不知是从哪裡拿出来的，梅诺特用温乎的湿毛巾温柔的擦拭克莉丝的脸，在说着婴儿用语的同时好乖好乖的摸着她的头。

虽然有些死板，但她是一位极为符合稳重这一形容的女性。
梅诺特不只育儿，就连战鬥多少都能做得来。
真是一位万能的乳母。

「好了，大小姐。
今天有尿尿吗~？」

不过，如此万能的，多半堪称精英的她口中说出的，却是用来哄小孩的肉麻的話。

股间附近感到了些许清凉，但克莉丝并不会去想自己被她做了什麼这样一点也不风雅的事。

「……」

没错，无论是有多麼害羞，无论是有多麼不情愿，克莉丝也绝不会开口说話。

就好比仅仅只是等待台风过去的芦苇一样，克莉丝也仅仅只是等待着结束的来临。

「嘛，大小姐今天也没问题呢！
梅诺特非常开心。」

「……」

梅诺特开心的笑着，用明朗的声音如此说道。

而与之相对克莉丝的反应非常的冷淡。

虽然对乳母梅诺特不太好意思，但克莉丝对於明显比自己强的人，不如说是对所有的大人都不擅长。

对患有对人恐惧症的克莉丝来说，比起温柔她更先感受到的是恐惧。每当梅诺特微微抬手，克莉丝就会身体一颤。

因为她虽然明知不会發生，但总觉得自己要被打了……

「啊啦啊啦，大小姐今天也不开心吗？」

见她身体各处都在颤抖，或许是想哄她开心吧，梅诺特笑着用指肚推着克莉丝那富有弹性的脸蛋。

「……」

但是，那果然也是一如既往的事了。

看着一味的僵直下去，没有任何反应的我，梅诺特好像放弃了一般说道。

「哈…大小姐。
好了，和梅诺特一起看看外面吧？」

「……」

梅诺特虽然说的是疑问句，但那却并没有要询问克莉丝想法的打算。

克莉丝被大大的手掌温柔的，缓慢的抱起，带到了窗户旁边。

「……」

视角变得比自己的身高高出许多。隔着窗子向外望去，能看见那湛蓝的天空和洁白的云。整理的十分漂亮的街道，活力四射的市场。还有那耀眼的日光。

只不过，那些虽然看起来很近但实际上都很远。

在前世能做到的事，对现在的克莉丝来说是做不到的。不用说是外出了，就连眺望窗外都无法自如做到。

透过窗子所见的一切，都是克莉丝无法触及的世界。

没错，只有这个宽敞的房间和梅诺特是她那狭窄的世界，是她的鸟笼。

「……」

我一言不发的，持续注视着窗外。

不知为何克莉丝未曾见过自己今世的父母。
就连有没有兄弟姐妹也不知道。

……我到底是为什麼，会被软禁在这个房间裡的呢？……没有见过我的家人，为什麼会被隔离起来了呢……？

疑问虽堆积如山，却没有答案。
幼小的她还什麼都不知道。

因为单凭从窗子看到的景色，以及梅诺特单方面诉说的语言所得到的知识，幾乎等同於什麼也没知道……

「已经是春天了哦~，阳光好暖和啊~」

「……」

听着梅诺特所说的話，我悄悄的以用魔力强化过的眼睛看向外面的世界。
但是，无论看了多少遍果然我对在那看到的街道没有印象。与在过去，克莉丝所生活过的王都相比，没有一丝共通之处。

……这裡是哪裡的国家呢，难道不是王都吗……？

「看啊，大小姐。好高好高~，随你怎麼看都可以哦~」

将自己抱在怀裡摇晃的梅诺特，对克莉丝抱有疑问的事，就连她使用魔法强化视力的事，都丝毫没有察觉……

「……」

已经完全对没有任何变化的日常不抱一丝希望的她（克莉丝），正盘算着为了夜裡的修行而温存体力……


　…………
　………
　……
　…


「……」

时间是深夜。
在没有任何气息的房间中，有一位突然起身的幼女。

她无言的擦了擦脸，强行撑开行将闭上的眼睛，为了不忘记说話的方法开始嘟囔起了惯例的自言自语。

「……婴儿…总是…在想…什麼……呢……？」

那听起来虽然好像很蠢，但对克莉丝来说却是一个很大的疑问。
虽然只限白天，但一年以上的时间仅仅只是一味的发呆，这着实有些难堪。

「……我能…很好的…说話…吗……？」

根本没有和其他人说过話的这一年。

时常缩在自己的壳里，以待有偏见的世界观看待世界的她，在前世还好，感觉自己变得不擅长和其他人说話了。

「……没问题…吗……？」

虽说如此，是否能很好的说話这一点，这是由他人的基準才能明白的，并不是自己一个人就能判断的了的。
放弃了提前思考的克莉丝，这次开始针对自己的社会立场研究起来。

「……大概…是贵族…但是……」

优秀的佣人。虽未把握全貌，但恐怕不小的宅邸。
总觉得外观很是奢华、高雅的家具。

从诸多的判断材料来看，虽然能预想到自己或许是贵族，但无论怎样总是会让厌恶感占据优势。

「……讨厌…贵族……」

关於贵族她虽然没有特别详细的知识，但在前世他因为仗势欺人的贵族子弟断送了性命，那之前也对他各种欺辱。
所以她不可能对贵族抱有好印象。

「……要是…那傢伙…来到……我面前…的話……」

说起贵族，最先想起的，是对克莉丝来说一年前。杀了前世的自己的金髮男子。

在旁人看来，说不定只能看见一位尚为孩童的幼女那匀称的面孔可爱的扭曲了起来，但寄宿在那瞳孔中的是不容置辩的憎恨。

即便是克莉丝，也没有宽容到能原谅杀死自己的人。

「……啊，我……！？」

不过，随後克莉丝便睁开了眼睛，独自一人在巨大的床上抱住自己柔弱的双肩，开始颤抖起来。

……我、我刚刚、在想些什麼……？

我知道我自己憎恨着他。也知道自己无法原谅他。但是，我想做的事却是……

克莉丝微微摇了摇头。就那样用数分钟时间在柔软的床上等待自己冷静下来。

「……讨厌…贵族……」

克莉丝的思考最终回归了原点。她无视了自己醜陋的一面。
那无可非议的是克莉丝的弱点。

「……变得…强大吧……为了…不让人…欺负……」

但是，克莉丝此时却积极的立下了一个决心。

若不想变得像前世那样的話，只要变强就好了。

这虽然是个极端的例子，但只要入手像童話裡出场的英雄那样的力量的話，自己也一定会对自己抱有自信了吧。


「……加油吧……」

至少只要有了普通人程度的力量的話，这份恐惧也会消失的吧。
恐惧消失了的話，只要变得能和别人说好多好多的話，自己就不会被欺负了吧。


「……强大……」

在这个世界裡，自己还远远只是一个弱者。
自己所有的，只是在前世与普通魔术师同等的魔力，相对较高的魔法技术，以及远低于他们的身体能力。

最主要的是在缺少绝对性力量的现在，即使找到了仇人，即使见到了贵族，自己也是束手无策。根本无法抵抗。

「……嗯……变强…的話……」

克莉丝有着注意自己不会做出像多少回忆起前世自己被双亲想垃圾一样卖掉、以奴隷的身份被人侮辱了身为人类的尊严的艰辛记忆那样，不慎重的行动的倾向。

现在也在想着自己要不让其他人看出诸如自己的魔力之高，自己的思考能力之高等明显与普通孩童不同的地方，隐藏着这些生活下去。

至少到得到可以自立的力量时为止……

「……只要…变强…的話……」

为了自己。为了女神大人。
幼女无言的闭上眼，操纵起多彩的魔法。

她的周围是那麼美丽。
描绘出複杂轨道飞行着的火球的数量正与日俱增，她闭上眼时所能探索的距离，这个宽敞的房间、她的鸟笼已经收容不下了。

「……我…很强…救世主……」

在心中描绘出最强的自己。
不被任何人欺负，被大家所喜爱、所珍视的理想的自己。

「……我……」

她自然的笑了。

今天似乎能做一个好梦……

===============================

《人物介绍》

克莉丝……主人公。现在马上就两岁了。现女，原伪娘。大小姐。

梅诺特……克莉丝的乳母。实际上是满脑子肌肉的类型。

