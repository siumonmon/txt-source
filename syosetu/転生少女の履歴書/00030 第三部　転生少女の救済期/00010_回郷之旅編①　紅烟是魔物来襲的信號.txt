贵族孩子们乘坐的马车就像大名的队伍一样，排成长蛇列前进的样子非常的壮观。
其中一台马车上放着我要带回卢比法尔的行李，我和光妈妈一起坐在马夫座上，虽然这是只有两个人的旅行，但因为周围都是其它领地的马车，结果还是相当的吵闹。

本来在我的马车上应该也会有一位陪同的护卫骑士，但怎麼说呢，该说是不出所料吗，完全没有一位骑士有愿意守护卢比法尔领马车的气魄，所以能以这种像是集体放学一样的形式回乡真是帮大忙了。
因为，如果只有我和妈妈两个人，要走完这条徘徊着魔物的归途，还是很辛苦的呢。

但是，随着旅途的前进，我觉得大家渐渐都会向着各自的领地离开队伍，这样的话，终归会轮到只剩我和光妈妈两个人的旅途。好可怕。
不过一般来说，因为只有两个人的旅程还是太勉强了，所以我预定会先前往雷恩福雷斯特雇佣新的护卫。虽然不知道能不能雇佣的到……。但是，雇佣不到话……。

「不对，不管怎麼说那还是有点难啊！」
与光妈妈商谈之後的预定时，不由地大声喊了出来。不过，光妈妈好像也是这麼觉得，沉重地点了下头。
「是呢。索性就这样抛弃马车，两个人骑马去？如果没有行李的话，就能缩短用时，也能减少遇到魔物的機率，就算遇到袭击也有可能利用马匹的速度完全逃走哦」
「唔—，但是，在行李裡放了很多相当重要度东西。可以的话我还是想留下来」
「是吗」
在光妈妈这麼回应时，马车外突然骚闹了起来。我从车窗探出头去，就看见从队伍的前方冒起一股红烟直冲云霄。
那是我为以防万一而送给坐在各马车中的学生们的彩色烟玉。红色的烟代表遭到了魔物的袭击。

「红色的烟。似乎魔物出现了。光妈妈，我要过去！」
「等等莉尤酱，我可不会让你一个人过去的哟！我也一起去！」
「诶，但是那样的话，就没有车夫……」
不管怎麼说，作为不人气领地排名第一的卢比法尔。根本就没有想参选护卫任务的骑士，所以只能自己担任车夫。
就、就算这样，我姑且还是伯爵千金……。

「我不会让你一个人去的哟！……啊！那边的那位，稍等一下！到这边来！」
光妈妈在这麼说着的同时，向正骑马走在前方不远处的骑士招呼道。
那是一位有着青黑相间的发色，绑着单马尾的女骑士。
在我前面的马车团体是前往雷恩福雷斯特的人们。艾伦乘坐的马车也在那些马车中。所以，在那其中骑马并排走着，一副骑士风格打扮的人应该就是护卫雷恩福雷斯特学生们的王国骑士。

「对对！那边的·那·位！能稍微过来一下吗？」
光妈妈一如往常地扭动着身子，邀请着王国骑士。
被叫到的骑士，则因为被忸怩作态地搭话而吓了一跳地看了过来。不知道卢比法尔的人找自己有什麼事，完全就是一副受到惊吓的样子。根本没必要害怕扭来扭去。大概吧。

「真是的！快点啦！快点过来啦。没什麼好害怕的哟。只要稍微看管一下马车就行。我们只是想在那期间拜借一下你的马哦」
不过就算光妈妈语气温柔，被邀請的王国骑士还是一副怀疑的表情。

渐渐不耐烦的光妈妈表情险恶了起来。

「真是没志气啊！算了你快点过来！只要一会儿，只是担任车夫的工作！你看，钱！会给你劳务费哦！」
咚、光妈妈用像是投球那样的声音喊道。
讨厌、好可怕。
被邀請的王国骑士脸色发青的点了下头，下马後坐到了车夫座上。

诶？这位骑士，就是坐在之前叫嚣着「快点做火柴！」的那个络腮鬍**旁边的那位骑士。原来成了雷恩福雷斯特的护卫骑士啊。

光妈妈将钱塞到骑士手裡後就和我一起骑到了骑士的马上。
感觉已经好久没和光妈妈一起骑马了。

我们骑马赶到发烟地点时，魔物已经被退治了。似乎是被附近的魔法使学生用魔法打倒的。
包围着已经全身着火的魔物，包含王国骑士在内的大家都很高兴。

但是还不能大意。魔物像这样出现，就说明附近的结界已经出现了裂痕。还有存在其它魔物的可能性非常高。
我仔细地观察周围。来回巡视，是否还有其它魔物，仰视天空，是否还有飞翔的魔物……。还有就是，正当我这麼想着往下看时发现了踪迹。在一位大意的一脸呆相的王国骑士脚下，地面在震动。
不对，已经……出现裂痕了？

「那裡的，快躲开！」
我在喊着的同时飞身下马。然後扑向一脸惊讶的骑士，将他扑向一旁。
好痛。
转瞬之间，从骑士原先所在的地面下伸出一只长有鼹鼠般锋利钩爪的手腕。手腕横向一挥想要抓住曾在地上东西，但是那裡已经谁都不在了。

我顺势借走被扑倒在地的骑士的優质长剑，毫不犹豫地插向似乎藏有什麼东西的地面。
叽——地一声，从地下發出了魔物不成声的呻吟。
但是似乎不是致命伤，还能感觉到地面在少许震动，我扔下仍然插着的长剑急忙抽身退回。
而我刚刚所在的地面再次伸出了一支像是鼹鼠的手腕。

「等等！莉尤酱！不能随便跑出去哟！很危险！」
光妈妈边说边骑着马将我提了起来，再次放到了马背上。光妈妈力气真大。

之後，在那期间魔法使的学生似乎咏唱了咒文，还插着剑的地面渐渐隆起，像鼹鼠一样的魔物从大地中被完全挤了出来。

魔物的左肩还插着剑，但仍挥动手腕挖掘地面想要隐藏到地下。为了阻止其行动我拔出一根箭，拉弓射向魔物的手腕。
其他的学生也纷纷把剑投向魔物，勇敢地进攻着。
与此同时，其中一位学生立即点燃一根火柴递给魔法使的学生，随後魔物立刻就被火魔法包裹住。在各种各样的攻击下已经无法动弹的魔物就这样被火焰焚烧殆尽。

搞定。
我四处张望确认是否还有其它魔物。
……魔物的身影和气息，已经感觉不到了。

魔物被讨伐後，「魔物！？没问题吗！？」从其它马车那络绎不绝地有学生们关心地过来问道。
实在是可靠的学园军势。

结果在那之後，魔物也没有出现，但是有魔物出现过就意味这附近的结界很可能出现了破损。
虽然必须立刻进行修復，但若是就这样停下学园军一部分的脚步的话，那部分学生到达领地的时间就会被推迟。因此现在就由出现魔物的斯皮利亚（スピーリア）公爵领的学生与负责该领护卫任务的王国骑士们前去修復结界。

加油啊！祝你们武运昌盛！

也就是说其它领地的学生会就这样朝各自的领地前进，这样的话，一同走过同一旅程的大家就要渐渐各奔东西，一思及此我就觉得非常难过。

当我和光妈妈一起回到自己的马车时，先前拜托担任临时车夫的王国骑士竟然说就让她继续担任车夫也没问题，真是心怀感谢的拜托你了。

虽然看起来有些战战兢兢的样子，但这不是也有相当优秀之处吗。
在进行火柴的交涉之际，因为你和失礼的络腮鬍**坐在一起，还想过要不要把你扔进黑名单，不过还是不把你放了黑名单了，嗯。
