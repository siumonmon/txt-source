# novel

- title: 誰にでもできる影から助ける魔王討伐
- title_zh: 誰都能做到的暗中協助魔王討伐
- author: 槻影
- source: https://kakuyomu.jp/works/1177354054880238351
- cover: https://images-na.ssl-images-amazon.com/images/I/81iY8SzcvtL.jpg
- publisher: kakuyomu
- date: 2018-01-17T23:00:45+08:00
- status: 連載中

## series

- name: 誰にでもできる影から助ける魔王討伐

## preface


```
これは……ビジネスだ
槻影

魔王クラノスが人類に宣戦布告して十年。
強大極まりないクラノスの軍に劣勢に立たされ、退っ引きならない状態に陥った王国は教会の持つ禁断の秘奥、英雄召喚の実施を決定した。

召喚された聖勇者、藤堂直継の栄えあるパーティメンバーにプリースト（ヒーラー）として選ばれた俺は、残りのパーティメンバーとして選ばれた二人、魔導師と剣士が原石であり、まだ第一線で戦える実力にない事に気づく。
果たして俺は魔王からの尖兵をしのぎ切り、勇者とその仲間達をレベルアップさせることはできるのか！？


＊＊＊＊
エンターブレインさんより書籍、三巻まで発売中。
それぞれ書き下ろし他ステータスなどが収録されていますのでよろしくお願いします。

また、ComicWalkerにて漫画版が連載中です。詳しくは近況ノートをご確認ください。

引き続き、Web版書籍版共々よろしくお願いします。
```

## tags

- node-novel
- kakuyomu
- カクヨムオンリー
- ヒーラー
- ファンタジー
- プリースト
- ライトノベル
- 勇者
- 異世界ファンタジー
- 魔王

# contribute

- 絨質墮菌
- 海市蜃楼谁不识
- 弒澪
- 飒君CONAN
- 

# options

## textlayout

- allow_lf2: true

# link

- [谁都能做到的暗中协助魔王讨伐](https://tieba.baidu.com/f?kw=%E8%B0%81%E9%83%BD%E8%83%BD%E5%81%9A%E5%88%B0%E7%9A%84%E6%9A%97%E4%B8%AD%E5%8D%8F%E5%8A%A9%E9%AD%94%E7%8E%8B%E8%AE%A8%E4%BC%90&ie=utf-8 "谁都能做到的暗中协助魔王讨伐")
- [我异世界的姐妹很不自重吧](https://tieba.baidu.com/f?kw=%E6%88%91%E5%BC%82%E4%B8%96%E7%95%8C%E7%9A%84%E5%A7%90%E5%A6%B9%E5%BE%88%E4%B8%8D%E8%87%AA%E9%87%8D&ie=utf-8 "我异世界的姐妹很不自重")
- [masiro.moe](http://masiro.moe/forum.php?mod=forumdisplay&fid=61&page=1)
- 



